<?php
//echo "<h1>Understanding Array</h1>";

//index, associative, multi dimensional

//$student1 = 'google';
//$student2 = 'yahoo';
//$student3 = 'facebook';

//$student = array('google', 'yahoo', 'facebook');//index array

//$student = [
//                  'abc'=>'google',
//                  'yahoo',
//                  'facebook',
//                  10=>'pinterest',
//                  'twitter'
//              ];//associative array

//$department = [
//                'cse' => 'faka',
//                'bba' => [
//                    'student'=>[
//                                'google',
//                                'yahoo'
//                            ]
//                ]
//              ];//associative array

//echo "<pre>";
//
//print_r($department['bba']['student'][0]);

//echo $student1;

echo "<h1>Understanding Foreach Loop</h1>";

//$student = array('google'=>['a','b'], 'yahoo', 'facebook', 'twitter');//index array

echo "<pre>";

//for ($i = 0; $i < count($student); $i++){
//    echo $student[$i]."<br>";
//}

$department = [
                    'cse' => 'abc',
                    'bba' => [
                        'student'=>[
                            'google',
                            'yahoo'
                        ],
                        'pondit'
                    ]
               ];//associative array

//foreach ($department['bba']['student'] as $onnokisu => $jkonokisu){
//    echo $onnokisu."  ".$jkonokisu."<br/>";
//}


$due_bill = [
    'Mr. Rahim' => 100,
    'Mr. Karim' => 95.5,
    'Mr. Akkas' => 195.5,

];

//$sum = 0;
//foreach ($due_bill as $k=>$v){
//    echo $k . "'s due is " . $v . "<br>";
//   $sum += $v;
//}
//echo "total due: $sum";


//for($i = 1; $i<=10;++$i ) :
//
//    echo $i . "<br>" ;
//
//endfor;


$z = 1;
$z = [];
$z = 5;

var_dump($z) ;

//print_r($student);
