<?php

echo '<h1>Understanding Foreach Loop</h1>';

//$students = ['google', 'facebook', 'yahoo'];//index array
//$students = ['student1' => 'google', 'student2' => 'facebook', 'student3' => 'yahoo'];//associative array
$departments = [
                 'cse'=>[
                     'student'=>[
                         'google',
                         'facebook',
                         'yahoo'
                     ],
                     'teacher'=>[
                         'youtube',
                         'microsoft',
                         'jkonokisu'
                     ]
                 ],
                 'bba'=>[
                     'student'=>[
                         'Kamal',
                         'Jamal',
                         'Rahim'
                     ],
                     'teacher'=>[
                         'roni',
                         'tapos',
                         'misuk'
                     ]
                 ]
              ];//multidimensional array

echo "<pre>";
//print_r($students);
//print_r($departments);

//foreach ($students as $index => $student){
//    echo $index.' '.$student.'<br/>';
//}

foreach ($departments as $key => $value){
    echo "<hr>";
    echo $key .'<br/>';
    echo "<hr>";
    foreach ($value as $key =>$items){
        echo "<hr>";
        echo $key .'<br/>';
        echo "<hr>";
        foreach ($items as $item){
            echo $item."<br>";
        }
    }
}


