<?php
class Department
{
//    public, private, protected
    public $public = 'This is public property';
    private $private = 'This is private property';
    protected $protected = 'This is protected property';

    private $password = '1234567';
    private $email = 'abcd@gmail.com';

    public function __construct($name = 'default')
    {
        echo 'Hello From '.$name;
    }

    private function auth()
    {
        if($this->email == 'abcd@gmail.com' && $this->password == 123456){
            return 'valid user';
        }else{
            echo 'Invalid user';
        }
    }

    public function login()
    {
        return $this->auth();
    }

    protected function getPrivateProperty()
    {
        return $this->private;
    }

    private function getPrivateMethod()
    {
        echo 'This output from Department Private method';
    }

    public function getMethods()
    {
        return $this->getPrivateMethod();
    }

}