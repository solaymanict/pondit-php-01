<?php
require_once('Department.php');
require_once('Student.php');

$department = new Department('cse');
echo "<br/>";
echo $department->public;
echo "<br/>";
echo $department->getMethods();
echo "<br/>";
echo $department->login();
echo "<hr/>";

$student = new Student('bba');
echo "<br/>";
echo $student->getProtectedProperty();

